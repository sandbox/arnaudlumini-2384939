<?php

class AWSJWPlayerApi {
  public static function get_smil($file_path, $secure = FALSE) {
    $smil = '<?xml version="1.0" encoding="UTF-8"?><smil>';

    $distribution = $secure ?
      variable_get('aws_jw_player_private_distribution', '??.cloudfront.net') :
      variable_get('aws_jw_player_public_distribution', '??.cloudfront.net');
    $distribution = 'rtmp://' . $distribution . ':1935/cfx/st/';

    $smil .= "<head><meta base='$distribution' /></head>";
    $smil .= '<body><switch>';

    $media_quality_names = explode(',', variable_get('aws_jw_player_media_quality_names', '2300,1500,900,700'));
    $video_heights = explode(',', variable_get('aws_jw_player_video_heights', '1080,720,480,360'));
    for ($i = 0; $i < count($media_quality_names); $i++) {
      $quality_name = trim($media_quality_names[$i]);
      $path = str_replace('*', $quality_name, $file_path);
      if ($secure) {
        $path = aws_get_policy_stream_name($path, FALSE);
      }
      $system_bitrate = $quality_name * 1000;
      $smil .= "<video src='$path' height='$video_heights[$i]' system-bitrate='$system_bitrate' />";
    }

    $smil .= '</switch></body></smil>';
    return $smil;
  }

  public static function get_direct_download_path($file_path, $secure = FALSE) {
    $distribution = $secure ?
      variable_get('aws_jw_player_private_direct_download_distribution', '??.cloudfront.net') :
      variable_get('aws_jw_player_public_direct_download_distribution', '??.cloudfront.net');
    $distribution = 'http://' . $distribution;

    $media_quality_names = explode(',', variable_get('aws_jw_player_media_quality_names', '2300,1500,900,700'));
    $quality_name = trim(end($media_quality_names));
    $path = str_replace('*', $quality_name, $file_path);
    $path = $distribution . '/' . $path . '?timestamp=' . time();
    if ($secure) {
      $path = aws_get_policy_stream_name($path, FALSE);
    }
    return $path;
  }

  public static function is_considered_mobile($headers) {
    $user_agent = $headers['User-Agent'];

    if (strpos($user_agent, 'iPhone') === FALSE && strpos($user_agent, 'iPad') === FALSE && strpos($user_agent, 'Android') === FALSE) {
      return FALSE;
    }
    else {
      return TRUE;
    }
  }

  public static function is_absolute_url($url) {
    if(strpos($url, '://') === FALSE){
      return FALSE;
    }
    return TRUE;
  }
}
