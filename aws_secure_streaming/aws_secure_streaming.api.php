<?php


function aws_rsa_sha1_sign($policy, $priv_key) {
    $signature = "";
	
    // load the private key
//    $fp = fopen($private_key_filename, "r");
//    $priv_key = fread($fp, 8192);
//    fclose($fp);
    $pkeyid = openssl_get_privatekey($priv_key);

    // compute signature
    openssl_sign($policy, $signature, $pkeyid);

    // free the key from memory
    openssl_free_key($pkeyid);

    return $signature;
}

//function aws_rsa_sha1_sign($policy, $private_key_filename) {
//    $signature = "";
//	
//	if(!is_file($private_key_filename)){
//		drupal_set_message('aws - private key file not found', 'error', false);
//		return '';
//	}
//	
//    // load the private key
//    $fp = fopen($private_key_filename, "r");
//    $priv_key = fread($fp, 8192);
//    fclose($fp);
//    $pkeyid = openssl_get_privatekey($priv_key);
//
//    // compute signature
//    openssl_sign($policy, $signature, $pkeyid);
//
//    // free the key from memory
//    openssl_free_key($pkeyid);
//
//    return $signature;
//}

function aws_url_safe_base64_encode($value) {
    $encoded = base64_encode($value);
    // replace unsafe characters +, = and / with the safe characters -, _ and ~
    return str_replace(
        array('+', '=', '/'),
        array('-', '_', '~'),
        $encoded);
}

function aws_create_stream_name($stream, $policy, $signature, $key_pair_id, $expires) {
    $result = $stream;
    // if the stream already contains query parameters, attach the new query parameters to the end
    // otherwise, add the query parameters
    $separator = strpos($stream, '?') == FALSE ? '?' : '&';
    // the presence of an expires time means we're using a canned policy
    if($expires) {
        $result .= $separator . "Expires=" . $expires . "&Signature=" . $signature . "&Key-Pair-Id=" . $key_pair_id;
    } 
    // not using a canned policy, include the policy itself in the stream name
    else {
        $result .= $separator . "Policy=" . $policy . "&Signature=" . $signature . "&Key-Pair-Id=" . $key_pair_id;
    }

    // new lines would break us, so remove them
    return str_replace('\n', '', $result);
}

function aws_encode_query_params($stream_name) {
    // the adobe flash player has trouble with query parameters being passed into it, 
    // so replace the bad characters with their url-encoded forms
	global $aws_encode_query_params;
	if ($aws_encode_query_params)
	{
		return str_replace(
        	array('?', '=', '&'),
        	array('%3F', '%3D', '%26'),
        	$stream_name);
	}
	else
	{
   		return $stream_name;
	} 
}

function aws_get_canned_policy_stream_name($video_path, $private_key, $key_pair_id, $expires) {
    // this policy is well known by CloudFront, but you still need to sign it, since it contains your parameters
    $canned_policy = '{"Statement":[{"Resource":"' . $video_path . '","Condition":{"DateLessThan":{"AWS:EpochTime":'. $expires . '}}}]}';
    // the policy contains characters that cannot be part of a URL, so we base64 encode it
    $encoded_policy = aws_url_safe_base64_encode($canned_policy);
    // sign the original policy, not the encoded version
    $signature = aws_rsa_sha1_sign($canned_policy, $private_key);
    // make the signature safe to be included in a url
    $encoded_signature = aws_url_safe_base64_encode($signature);

    // combine the above into a stream name
    $stream_name = aws_create_stream_name($video_path, null, $encoded_signature, $key_pair_id, $expires);
    // url-encode the query string characters to work around a flash player bug
    return aws_encode_query_params($stream_name);
}

function aws_get_custom_policy_stream_name($video_path, $private_key, $key_pair_id, $policy) {
    // the policy contains characters that cannot be part of a URL, so we base64 encode it
    $encoded_policy = aws_url_safe_base64_encode($policy);
    // sign the original policy, not the encoded version
    $signature = aws_rsa_sha1_sign($policy, $private_key);
    // make the signature safe to be included in a url
    $encoded_signature = aws_url_safe_base64_encode($signature);

    // combine the above into a stream name
    $stream_name = aws_create_stream_name($video_path, $encoded_policy, $encoded_signature, $key_pair_id, null);
    // url-encode the query string characters to work around a flash player bug
    return aws_encode_query_params($stream_name);
}

function aws_get_policy_stream_name($filepath, $encode_query_params = true)
{
	global $aws_encode_query_params;
	$aws_encode_query_params = $encode_query_params;
	$expires = time() + variable_get('aws_secure_streaming_url_expire', 300);
	return aws_get_canned_policy_stream_name($filepath, variable_get('aws_secure_streaming_private_key', ''), variable_get('aws_secure_streaming_key_pair_id',''), $expires);
//	$file = file_load(variable_get('aws_secure_streaming_private_key_file', ''));
//	if(!$file){
//		drupal_set_message('aws - private key file not found', 'error', false);
//		return '';
//	}
//	return aws_get_canned_policy_stream_name($filepath, $file->uri, variable_get('aws_secure_streaming_key_pair_id',''), $expires);
}
