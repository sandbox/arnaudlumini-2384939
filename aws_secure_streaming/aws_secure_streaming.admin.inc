<?php

function aws_secure_streaming_admin_form() {
  $form['aws_secure_streaming_private_key'] = array(
    '#type' => 'textarea',
    '#title' => t('Private key content :'),
    '#description' => t("Type the private Key content."),
    '#default_value' => variable_get('aws_secure_streaming_private_key', ''),
  );
  $form['aws_secure_streaming_key_pair_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Key Pair id :'),
    '#description' => t("Type the Key Pair id."),
    '#default_value' => variable_get('aws_secure_streaming_key_pair_id', ''),
  );
  $form['aws_secure_streaming_url_expire'] = array(
    '#type' => 'textfield',
    '#title' => t('Expires :'),
    '#description' => t("Type the seconds validity time ex : 300 for 5 mins."),
    '#default_value' => variable_get('aws_secure_streaming_url_expire', 300),
  );
  $form = system_settings_form($form);
  return $form;
}
