<?php

/**
 * @file
 * Hooks provided by the AWS JW Player module.
 */

/**
 * Alters the view.
 *
 * @param $entity
 *  The entity being displayed.
 * @param $element
 *  A renderable array for the $items, as an array of child elements keyed
 *  by numeric indexes starting from 0.
 * @return $element
 *  The altered element.
 */
function hook_aws_jw_player_field_formatter_view_alter($entity, &$element) {
}
