<?php

function aws_jw_player_settings_form($form, &$form_state) {
  $form['videos'] = array(
    '#type' => 'fieldset',
    '#title' => t('Videos'),
    '#description' => t('Comma separated values in a descending order.'),
    '#prefix' => '<div id="aws-jw-settings-videos">',
    '#suffix' => '</div>',
  );

  $form['videos']['aws_jw_player_use_smil'] = array(
    '#type' => 'checkbox',
    '#title' => t('Use smil'),
    '#default_value' => variable_get('aws_jw_player_use_smil', TRUE),
    '#ajax' => array(
      'callback' => 'aws_jw_player_settings_form_smil_callback',
      'wrapper' => 'aws-jw-settings-videos',
    )
  );
  if((isset($form_state['values']['aws_jw_player_use_smil']) && $form_state['values']['aws_jw_player_use_smil']) || variable_get('aws_jw_player_use_smil', TRUE)){
    $form['videos']['aws_jw_player_media_quality_names'] = array(
      '#type' => 'textfield',
      '#title' => t('Media quality names :'),
      '#default_value' => variable_get('aws_jw_player_media_quality_names', '2300,1500,900,700'),
    );

    $form['videos']['aws_jw_player_bandwidth_bounds'] = array(
      '#type' => 'textfield',
      '#title' => t('Bandwidth bounds :'),
      '#default_value' => variable_get('aws_jw_player_bandwidth_bounds', '3000,2300,1500,900'),
    );

    $form['videos']['aws_jw_player_video_heights'] = array(
      '#type' => 'textfield',
      '#title' => t('Video heights :'),
      '#default_value' => variable_get('aws_jw_player_video_heights', '1080,720,480,360'),
    );
  }

  $form['public'] = array(
    '#type' => 'fieldset',
    '#title' => t('Public distribution domains'),
  );

  $form['public']['aws_jw_player_public_distribution'] = array(
    '#type' => 'textfield',
    '#title' => t('Public RTMP distribution domain'),
    '#description' => t('???.cloudfront.net'),
    '#default_value' => variable_get('aws_jw_player_public_distribution', '??.cloudfront.net'),
  );

  $form['public']['aws_jw_player_public_direct_download_distribution'] = array(
    '#type' => 'textfield',
    '#title' => t('Public direct download distribution domain'),
    '#description' => t('??.cloudfront.net'),
    '#default_value' => variable_get('aws_jw_player_public_direct_download_distribution', '??.cloudfront.net'),
  );

  if (module_exists('aws_secure_streaming')) {
    $form['private'] = array(
      '#type' => 'fieldset',
      '#title' => t('Private distribution domains'),
    );

    $form['private']['aws_jw_player_private_distribution'] = array(
      '#type' => 'textfield',
      '#title' => t('Private RTMP distribution domain'),
      '#description' => t('Type the private streaming distribution domain'),
      '#default_value' => variable_get('aws_jw_player_private_distribution', '??.cloudfront.net'),
    );
    $form['private']['aws_jw_player_private_direct_download_distribution'] = array(
      '#type' => 'textfield',
      '#title' => t('Private direct download distribution domain'),
      '#description' => t('Type the private direct download distribution domain'),
      '#default_value' => variable_get('aws_jw_player_private_direct_download_distribution', '??.cloudfront.net'),
    );
  }

  $form = system_settings_form($form);
  return $form;
}

function aws_jw_player_settings_form_smil_callback($form, &$form_state) {
  return $form['videos'];
}