<?php

function _aws_jw_player_get_smil($field_value) {
  $field_value = drupal_base64_decode(str_replace('.smil', '', $field_value));
  $smil = AWSJWPlayerApi::get_smil($field_value);
  echo $smil;
  drupal_exit();
}

function _aws_jw_player_get_secure_smil($field_value) {
  $field_value = drupal_base64_decode(str_replace('.smil', '', $field_value));
  $smil = AWSJWPlayerApi::get_smil($field_value, TRUE);
  echo $smil;
  drupal_exit();
}
